# standard_project

>A single sentance summarising the project and what it's for.

A longer description of the project and why it's needed, can be multiple paragraphs if needed.

## Prerequisites

Any software or assumptions the project needs to run.

## Installation

Any steps required to install the project.

## Usage

This should contain information about how to use the project, whether it be docker commands, python imports or instructions on how to build the binaries.

### Environment Variables

Any environment variables the code looks for, could also be docker environment variables or username/password info that the code expects.

## Built With

* A list
* of software projects
* and dependencies

## Links

A list of links to the project's pages on the various internal tools.

* [GitLab](https://gitlab.com/piatra-automation/standard_project)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the
[tags on this repository](https://gitlab.com/piatra-automation/standard_project/-/tags).

## Authors

* **Peter Kalt** - [piatra-automation](https://gitlab.com/piatra-automation)
* **Daniel Vonthethoff** - [dv-enigma](https://gitlab.com/dv-enigma)
