#!/usr/bin/env python3
"""
``standard_project`` is a template repository for Python Git.
The files in this src directory are arranged according to PEP practices.

.. moduleauthor:: Peter Kalt <peter.kalt@piatra.com.au>

"""

# pylint: disable=W0613, W0603, W0601, E0401

import argparse
import logging
import os
from collections import ChainMap

CONFIG: ChainMap
_DEFAULT = argparse.Namespace(in_dir=None, out_dir=None)
COMMAND_LINE_ARGS = _DEFAULT
LOGGER = logging.getLogger(__file__)


def start_logger():
    """Set the LOGGER parameters...
    everything is sent to the LOGFILE,
    but only warnings or higher are sent to the console.
    """
    # pylint: disable=E1101

    global LOGGER
    global CONFIG

    # Add some custom logging levels
    add_logging_level("RESULT", logging.WARNING - 3)
    add_logging_level("ACTION", logging.INFO + 5)
    add_logging_level("TODO", logging.INFO + 3)
    add_logging_level("VERBOSE", logging.DEBUG - 5)

    # Start the root LOGGER
    # LOGGER = logging.getLogger(filename)
    LOGGER.setLevel(logging.VERBOSE)

    # Create and configure CONSOLE LOGGER
    console = logging.StreamHandler()
    console.setLevel(logging.VERBOSE)
    formatter = logging.Formatter("%(name)-12s: %(levelname)-8s %(message)s")
    console.setFormatter(formatter)
    # If the INSIDE_DOCKER environment variable is set then we are inside
    # a Docker container and we dont add in console logging.
    if not os.environ.get("INSIDE_DOCKER", False):
        LOGGER.addHandler(console)

    # Announce the start of logging
    LOGGER.debug("LOGGER started")


def add_logging_level(level_name, level_num, method_name=None):
    """
    Comprehensively adds a new logging level to the `logging` module and the
    currently configured logging class.

    `level_name` becomes an attribute of the `logging` module with the value
    `level_num`. `method_name` becomes a convenience method for both `logging`
    itself and the class returned by `logging.getLoggerClass()` (usually just
    `logging.Logger`).
    If `method_name` is not specified, `level_name.lower()` is used.

    To avoid accidental clobberings of existing attributes, this method will
    raise an `AttributeError` if the level name is already an attribute of the
    `logging` module or if the method name is already present

    Example
    -------
    >>> add_logging_level('TRACE', logging.DEBUG - 5)
    >>> logging.getLogger(__name__).setLevel("TRACE")
    >>> logging.getLogger(__name__).trace('that worked')
    >>> logging.trace('so did this')
    >>> logging.TRACE
    5
    """

    # pylint: disable=W0212

    if not method_name:
        method_name = level_name.lower()

    if hasattr(logging, level_name):
        raise AttributeError("{} exists in logger".format(level_name))
    if hasattr(logging, method_name):
        raise AttributeError("{} exists in logger".format(method_name))
    if hasattr(logging.getLoggerClass(), method_name):
        raise AttributeError("{} exists in LOGGER".format(method_name))

    # This method was inspired by the answers to Stack Overflow post
    # http://stackoverflow.com/q/2183233/2988730, especially
    # http://stackoverflow.com/a/13638084/2988730
    def log_for_level(self, message, *args, **kwargs):
        if self.isEnabledFor(level_num):
            self._log(level_num, message, args, **kwargs)

    def log_to_root(message, *args, **kwargs):
        logging.log(level_num, message, *args, **kwargs)

    logging.addLevelName(level_num, level_name)
    setattr(logging, level_name, level_num)
    setattr(logging.getLoggerClass(), method_name, log_for_level)
    setattr(logging, method_name, log_to_root)


def main():
    """The main program loop exported to command line"""
    global CONFIG
    global LOGGER

    start_logger()
    LOGGER.critical("Program does nothing yet")


if __name__ == "__main__":
    main()
