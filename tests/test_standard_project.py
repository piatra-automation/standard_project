#!/usr/bin/env python3
""" tests for standard_project """

from src.standard_project import __version__


def test_version():
    """test version number matches expectations"""
    assert __version__ == "0.1.0"
