ENV = /usr/bin/env
VERSION := $(shell git describe --always)

# Set the project name if it's empty
ifeq ($(strip $(PROJECT_NAME)),)
PROJECT_NAME = $(shell basename -s .git `git config --get remote.origin.url`)
endif

.ONESHELL: ; # recipes execute in same shell
.NOTPARALLEL: ; # wait for this target to finish
.EXPORT_ALL_VARIABLES: ; # send all vars to shell

.PHONY: all, docs # All targets are accessible for user
.DEFAULT: help # Running Make will run the help target

help: ## Show Help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

install: ## Install project dependencies
	poetry self -V || \
		{ source $$HOME/.poetry/env && poetry self -V; } || \
		{ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_PREVIEW=1 python3 && \
			source $$HOME/.poetry/env; }
	poetry install --no-dev

install-dev: ## Install dev environment dependencies
	poetry self -V || \
		{ source $$HOME/.poetry/env && poetry self -V; } || \
		{ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_PREVIEW=1 python3 && \
			source $$HOME/.poetry/env; }
	poetry install

lint-code: ## Automatically lint and format the custom components
	poetry run autoflake --remove-all-unused-imports --recursive --remove-unused-variables --in-place src/standard_project --exclude=__init__.py
	poetry run isort --multi-line=3 --trailing-comma --force-grid-wrap=0 --combine-as --line-width 88 src/standard_project
	poetry run black -S src/standard_project

scan-code: ## Scan the custom components and flag errors
	poetry run vulture src/standard_project --min-confidence 100
	poetry run mypy --ignore-missing-imports src/standard_project

clean: ## Remove all temporary files and build cache

test: ## Run automated tests

build: ## Build the code

docs: ## Build and publish the documentation
	poetry run $(MAKE) -C docs html
